import bpy
bl_info = {
    "name": "Copy Version Info",
    "author": "Sun Kim",
    "version": (1, 0),
    "blender": (2, 80, 0),
    "location": "Status Bar",
    "description": "Adds a button to the status bar to copy the version information of Blender",
    "warning": "",
    "doc_url": "",
    "category": "System",
}


class CopyVersionInfo(bpy.types.Operator):
    version_info = "version: " + bpy.app.version_string + ", branch: " + bpy.app.build_branch.decode(
        'utf-8', 'replace') + ", commit date: " + bpy.app.build_commit_date.decode(
        'utf-8', 'replace') + " " + bpy.app.build_commit_time.decode(
            'utf-8', 'replace') + ", hash: `" + bpy.app.build_hash.decode('ascii') + "`"

    bl_idname = "wm.copy_version_info"
    bl_label = "Press this button to copy version info below:"
    bl_description = version_info

    def execute(self, context):
        context.window_manager.clipboard = self.version_info
        self.report({'INFO'}, f"Copied to the clipboard")
        return {'FINISHED'}


def draw_copy_version_info_button(self, context):
    layout = self.layout
    layout.operator("wm.copy_version_info", text="", icon='COPYDOWN')


def register():
    bpy.utils.register_class(CopyVersionInfo)
    bpy.types.STATUSBAR_HT_header.append(draw_copy_version_info_button)


def unregister():
    bpy.utils.unregister_class(CopyVersionInfo)
    bpy.types.STATUSBAR_HT_header.remove(draw_copy_version_info_button)


if __name__ == "__main__":
    register()
