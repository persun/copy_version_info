# copy_version_info

A simple Blender script to add a button to the status bar to check and copy version info.

![Screenshot](screenshot.png)
